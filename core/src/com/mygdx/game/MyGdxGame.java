package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Logger;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	private Logger logger;
	@Override
	public void create () {
		logger = new Logger("Logger", Logger.INFO);
		logger.info("in create()");
		batch = new SpriteBatch();

		img = new Texture("badlogic.jpg");




	}

	@Override
	public void render () {
		//logger.info("in render()");  // this will flood the app with messages
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
	}
	
	@Override
	public void dispose () {
		logger.info("in dispose()");
		batch.dispose();
		img.dispose();
	}
}
