package com.mygdx.game;


import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.scenes.scene2d.Stage;




public class BugCollector extends Game
{
    private SpriteBatch batch;



    private Texture winMessageTexture;
    private boolean win;


    private Ducky duck1;
    private Pig pig;
    private Bug bug;

    private Stage mainStage;

    @Override
    public void create()
    {
        batch = new SpriteBatch();
        winMessageTexture = new Texture("win.jpg");


        duck1 = new Ducky();
        duck1.setTexture(new Texture("ducky.png"));
        duck1.setPosition(20,20);


        pig = new Pig();
        pig.setTexture(new Texture("pig.png"));
        pig.setPosition(50,50);


        bug = new Bug();
        bug.setTexture(new Texture("bug.png"));
        bug.setPosition(240,250);


        mainStage = new Stage();

        mainStage.addActor(duck1);
        mainStage.addActor(pig);
        mainStage.addActor(bug);

        win = false;
    }

    public void render()
    {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if ( pig.overlaps(bug))
        {
            win = true;
        }

        if ( duck1.overlaps(bug))
        {
            win = true;
        }


        mainStage.act(1/60.0f); // checks for movement  and updates position, pass in frames per/sec.   lower it later if logic is slow

        batch.begin();
        if ( !win )
        {
            mainStage.draw();  // draws all actors in the stage in FIFO order
        }
        else
        {
            batch.draw(winMessageTexture,180,180);
        }
        batch.end();


    }

}
